import java.util.Arrays;

public class CodewarsMain {

	public static void main(String[] args) {
		
// FirstClass - simple sum of elements
//		FirstClass fc = new FirstClass();
//		System.out.println(fc.sum(2, 3));

// ArrayPrinter - simple string printer
//		ArrayPrinter ap = new ArrayPrinter();
//		Object[] array = {"h","o","l","a"};
//		
//		System.out.println(ap.printArray(array));
		
// Delete last zero from the given number
//		DeleteZeros dz = new DeleteZeros();
//		System.out.println(dz.noToZeros(1450));
	
// Array, where the first element is the count of positives and the second is the sum of negatives 
	
//		int[] input = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -11, -12, -13, -14, -15};
//		System.out.println(Arrays.toString(Kata.countPositivesSumNegatives(input)));
		
// Primitive calculator for the tips
		
//		System.out.println(TipCalculator.calculateTip(30d, "great"));
			
// Simple switch instruction
//		System.out.println(SwitchItUp.switchItUp(1));
		
// String switching
//		System.out.println(StringUtils.toAlternativeString("ThiS Is A Test"));
		
// Fun with arrays
//		int[][] arrayOfArrays = new int[][] { new int[] { 1, 2, 3, 4, 5, 6 }, new int[] { 7, 7, 7, 7, 7, 7 } };
//		System.out.println(AddingArrays.addingShifted(arrayOfArrays, 2));

// Eliminating number 5
		
		System.out.println(DontGiveMeFive.dontGiveMeFive(-6, 9));
		
	}

}
