
public class StringUtils {
	
	 public static String toAlternativeString(String string) {
		
		 StringBuilder sb = new StringBuilder(string);
		 for(int i = 0; i < sb.length(); i++) {
			 char c = sb.charAt(i);
			 if(Character.isLowerCase(c)) {
				 sb.setCharAt(i, Character.toUpperCase(c));
			 } else {
				 sb.setCharAt(i, Character.toLowerCase(c));
			 }
		 }
		 
		 return sb.toString();
		    
	  }

}
